#!/bin/bash
oc status
set -e

echo "Building"

docker build \
  -f ./ContainerFile \
  -t ${CI_REGISTRY_IMAGE}/django-alpine-js-dashboards-exploration:latest
from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.
# this set up was used because a plant's Sunlight can only be one of these 6 options but it can also have multiple sunlight options
class PlantSunlight(models.Model):
    class Amount(models.TextChoices):
        FULL_SHADE = "full shade", _("Full Shade")
        PART_SHADE = "part shade", _("Part Shade")
        PART_SUN = "part sun", _("Part Sun")
        FULL_SUN = "full sun", _("Full Sun")
        FILTERED_SHADE = "filtered shade", _("Filtered Shade")
        PART_SUN_PARD_SHADE = "part sun/part shade", _("PART SUN/PART SHADE")
        OTHER = "other", _("Other")

    amount = models.CharField(primary_key=True, max_length=25, choices=Amount.choices)


class Plant(models.Model):
    class CYCLE_CHOICES(models.TextChoices):
        Prennial = "Perennial", _("Prennial")
        ANNUAL = "Annual", _("Annual")
        Biennial = "Biennial", _("Biennial")
        Biannual = "Biannual", _("Biannual")


    class WATERING_CHOICES(models.TextChoices):
        FREQUENT = "Frequent", _("Frequent")
        AVERAGE = "Average", _("Average")
        Minimum = "Minimum", _("Minimum")
        NONE = "None", _("None")

    common_name = models.CharField(max_length=50)
    watering = models.CharField(max_length=25, choices=WATERING_CHOICES)
    sunlight = models.ManyToManyField(
        PlantSunlight, null=True
    )
    image = models.URLField(null=True, blank=True)
    cycle = models.CharField(max_length=25, choices=CYCLE_CHOICES, blank=True, null=True)


class PlantOtherName(models.Model):
    name = models.CharField(max_length=100)
    plant = models.ForeignKey(
        Plant,
        on_delete=models.CASCADE
    )


class PlantScientificName(models.Model):
    name = models.CharField(max_length=100, unique=True)
    plant = models.ForeignKey(
        Plant,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

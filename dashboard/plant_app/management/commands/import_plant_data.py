from django.core.management.base import BaseCommand
import json
import requests
from environs import Env


env = Env()
env.read_env() 
PERENUAL_API_KEY = env("PERENUAL_API_KEY")

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        response = requests.get("https://perenual.com/api/species-list?key=sk-oqIj657de69f263bb3483")
        content = json.loads(response.content)

        data = json.dumps(content)

        with open("./plant_app/management/commands/data.txt", "w") as file: # open a text file in write mode
            file.write(data) 

            file.close()
from plant_app.models import Plant
from django.core.management.base import BaseCommand
import datetime



class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        start = datetime.datetime.now()
        plants = Plant.objects.prefetch_related("sunlight").all()



        for plant in plants:
            h = plant.sunlight
            h_list = h.all()
            print(h_list)

        end = datetime.datetime.now()

        difference = end - start
        milliseconds = int(difference.total_seconds() * 1000)
        print(milliseconds)
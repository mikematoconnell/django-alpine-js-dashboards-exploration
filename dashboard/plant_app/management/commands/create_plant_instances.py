import datetime
import json

from django.core.management.base import BaseCommand
from django.db import transaction

from dashboard.plant_app.models import (
    Plant,
    PlantOtherName,
    PlantScientificName,
    PlantSunlight,
)


class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **kwargs):
        database_plants = Plant.objects.all().count()
        if database_plants == 0:
            full_shade, _ = PlantSunlight.objects.get_or_create(amount=PlantSunlight.Amount.FULL_SHADE)
            part_shade, _ = PlantSunlight.objects.get_or_create(amount=PlantSunlight.Amount.PART_SHADE)
            part_sun, _ = PlantSunlight.objects.get_or_create(amount=PlantSunlight.Amount.PART_SUN)
            full_sun, _ = PlantSunlight.objects.get_or_create(amount=PlantSunlight.Amount.FULL_SUN)
            filtered_shade, _ = PlantSunlight.objects.get_or_create(amount=PlantSunlight.Amount.FILTERED_SHADE)
            part_sun_part_shade, _ = PlantSunlight.objects.get_or_create(amount=PlantSunlight.Amount.PART_SUN_PARD_SHADE)

            # this allows for quick look up of each instance above in the list comprehension set up below to add the sunlight in a bulk create
            sun_light_types = {
                "full shade": full_shade,
                "part shade": part_shade,
                "part sun": part_sun,
                "full sun": full_sun,
                "filtered shade": filtered_shade,
                "part sun/part shade": part_sun_part_shade
            }

            # similar to the above really this is likely the better option but wanted to show two ways of settting it up
            # the above is a little easier to read but does not follow the DRY principle
            cycle_choices = Plant.CYCLE_CHOICES
            watering_choices = Plant.WATERING_CHOICES


            start = datetime.datetime.now()
            with open("./dashboard/plant_app/management/commands/data.txt", "r") as file: # open a text file in write mode
                content = file.readlines()


                data = json.loads(content[0])["data"]

                plants = [Plant(
                    common_name=p["common_name"], 
                    cycle=cycle_choices(p["cycle"]), 
                    watering=watering_choices(p["watering"])) 
                    for p in data]
                
                created_plants = Plant.objects.bulk_create(plants)

                len_plants = range(len(data))

                # https://docs.djangoproject.com/en/5.0/topics/db/optimization/#insert-in-bulk

                plant_sunlight_relationship = Plant.sunlight.through

                sunlight_field_plants = [plant_sunlight_relationship(
                    plant=created_plants[i], 
                    plantsunlight=sun_light_types[sunlight_type.lower()]) 
                    for i in range(len(data)) 
                        for sunlight_type in data[i]["sunlight"]] 

                plant_sunlight_relationship.objects.bulk_create(
                    sunlight_field_plants,
                    ignore_conflicts=True
                )

                other_name_plants = [
                    PlantOtherName(plant=created_plants[i], name=plant_other_name) 
                    for i in len_plants 
                    for plant_other_name in data[i]["other_name"]
                    ]
                scientific_name_plants = [
                    PlantScientificName(plant=created_plants[i], name=plant_scientific_name) 
                    for i in len_plants 
                    for plant_scientific_name in data[i]["scientific_name"]
                    ]

                PlantOtherName.objects.bulk_create(
                    other_name_plants
                )

                PlantScientificName.objects.bulk_create(
                    scientific_name_plants
                )

                # unoptimated
                
                # for plant in data:
                #     created_plant = Plant.objects.create(
                #         common_name=plant.get("common_name"),
                #         cycle=cycle_choices(plant["cycle"])
                #     )
                #     for type in plant["sunlight"]:
                #         created_plant.sunlight.add(type.lower())
                #     for scientific_name in plant["scientific_name"]:
                #         PlantScientificName.objects.create(
                #             plant=created_plant,
                #             name=scientific_name
                #         )
                #     for other_name in plant["other_name"]:
                #         PlantOtherName.objects.create(
                #             plant=created_plant,
                #             name=other_name
                #         )
                #     created_plant.save()


                file.close()
            
            end = datetime.datetime.now()
            difference = end - start
            milliseconds = int(difference.total_seconds() * 1000)
            print("time", milliseconds)


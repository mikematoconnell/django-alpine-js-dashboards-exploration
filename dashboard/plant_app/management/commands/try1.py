from django.core.management.base import BaseCommand
import json
from plant_app.models import Plant, PlantSunlight, PlantOtherName, PlantScientificName
import datetime
from django.db import transaction


class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **kwargs):
        Plant.objects.create(
            common_name="fsd",
            cycle="fsd"
        )
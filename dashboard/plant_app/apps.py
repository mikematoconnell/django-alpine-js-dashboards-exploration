from django.apps import AppConfig


class PlantAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dashboard.plant_app'

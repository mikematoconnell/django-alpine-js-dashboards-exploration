from django.apps import AppConfig


class DashboardAlpineConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dashboard.dashboard_alpine'

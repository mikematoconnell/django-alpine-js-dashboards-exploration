from django.urls import path

from dashboard.dashboard_alpine import views

urlpatterns = [
    path("", views.index, name="index"),
]

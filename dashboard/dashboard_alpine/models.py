from django.db import models

from dashboard.accounts.models import CustomUser


# Create your models here.
class Dashboard(models.Model):
    user = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE
    )

    class Meta:
        permissions = [
            ("view_admin_dashboard", "Can view admin dashboard")
        ]
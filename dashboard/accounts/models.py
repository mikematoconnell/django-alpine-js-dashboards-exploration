from django.contrib.auth.models import AbstractUser
from django.db import models

from dashboard.common.models import SoftDeleteMixin


# Create your models here.
class CustomUser(AbstractUser, SoftDeleteMixin):
    pass
    
December 16th, 2023

- using list comprehension is quicker than using regular for loops due to the underlying CPython library


NOTE FROM BING:
Yes, you can use django’s bulk_create when using foreign keys on the model, but you need to make sure that the foreign key values are already assigned to the objects before calling bulk_create. For example, if you have a model called Credit that has a foreign key to another model called Account, you can do something like this:

Python
AI-generated code. Review and use carefully. More info on FAQ.

accounts = Account.objects.all() # get the existing accounts
credits = [Credit(account=account, amount=100) for account in accounts] # create credit objects with foreign key values
Credit.objects.bulk_create(credits) # bulk create the credit objects
Alternatively, you can also use the primary key values of the foreign keys instead of the model instances, like this:

Python
AI-generated code. Review and use carefully. More info on FAQ.

accounts = Account.objects.all() # get the existing accounts
credits = [Credit(account_id=account.id, amount=100) for account in accounts] # create credit objects with foreign key values
Credit.objects.bulk_create(credits) # bulk create the credit objects
However, if you want to create both the foreign key objects and the related objects in bulk, you need to use a different approach. One way is to pre-generate the primary key values for the foreign key objects, and then use them to create the related objects. For example, if you want to create both Account and Credit objects in bulk, you can do something like this:

Python
AI-generated code. Review and use carefully. More info on FAQ.

last_account_id = Account.objects.last().id + 1000 # get the last account id and add some offset to avoid duplicates
accounts = [Account(id=i, name="Test") for i in range(last_account_id, last_account_id + 1000)] # create account objects with pre-generated ids
Account.objects.bulk_create(accounts) # bulk create the account objects
credits = [Credit(account_id=i, amount=100) for i in range(last_account_id, last_account_id + 1000)] # create credit objects with the same ids as the accounts
Credit.objects.bulk_create(credits) # bulk create the credit objects
Another way is to use the bulk_create method twice, and then query the foreign key objects by their order and assign them to the related objects. For example, you can do something like this:

Python
AI-generated code. Review and use carefully. More info on FAQ.

accounts = [Account(name="Test") for _ in range(1000)] # create account objects without ids
Account.objects.bulk_create(accounts) # bulk create the account objects
accounts = Account.objects.all().order_by('-id')[:1000][::-1] # get the newly created accounts by their order
credits = [Credit(account=account, amount=100) for account in accounts] # create credit objects with the accounts
Credit.objects.bulk_create(credits) # bulk create the credit objects
For more information, you can check out the Django documentation on related objects reference, or some of the web search results123 that I found for your query. I hope this helps! 😊



- completed the frist steps of the craete_plant_instances 
  - imported and saved 30 instances to a txt file
  - set up a sunlight field that is a many-to-many field
    - first had to create the Plant model instances and then I looped through the sunlight list from the API and added each sunlight type using plant_instance.sunlight.add(sunlight_type)
    - took about 11 seconds for 30 instances
    - used list comprehension which uses CPython and therefore is faster to create the Plant model instances
    - set up the sunlight model as only have an amount which is also the primary key
      - setting this up as the primary key allows me to get the instance by the amount (basically a name such as "full shade") rather than using an id number
    - following the documentation here https://docs.djangoproject.com/en/5.0/topics/db/optimization/#insert-in-bulk I was able to use list comprehension to create a list of Plant_sunglight model instances (these are instances of the relationship between plant model instance and sunlight)
      - **this took the time from 40 miliseconds to 22 milliseconds!!!**

January 3rd, 2024
- ajdusted the model slightly

January 6th, 2024
- set up the PlantScientifcName model which is a model with a foreign key to Plant, each plant may have multiple scientific names similar to the PlantOtherName model
- set up the CYCLE_CHOICES option and had to capitalize the values as they are capitilized in the api
  - imported the CYCLE_CHOICES in the create_plant_instances and was used with the following code to ensure that all fields followed the fields given in CYCLE_CHOICES
  - if this was not done any value could be added to the field as it was treated as a normal CharField, the choices option the field I believe refers more to using django forms whereas with import the plant data this is obviously not used
  cycle=cycle_choices(p["cycle"])

- results of optimization for 30 plants:
  - after computer restart optimized version runs in 6 milliseconds (I think before there were other programs slowing down my computer)
  - unoptimized runs in 35 miliseconds

January 7th and 8th, 2024
- looked into ways of building the application as a docker image and also of deploying the image to OpenShift
  - looked at the option of setting up a GitLab runner
  - looked at the option of setting up a Docker container that had then installed podman and the oc tool 
  - after looking through a bunch of documentation and asking AI some questions I was able to coble together a solution of installing podman and the oc tool into the default GitLab runner

January 17th, 2024
- slightly changed the code in the create_plant_instances to make it more readable
- set up static and staticfiles
- configured tailwind using CLI using the documentation found here: https://tailwindcss.com/blog/standalone-cli
- next steps: build a login page

January 24th, 2024
- set up a base abstract model which will handles soft deletion, capturing the user who create/updated/modified the model instance
  - disclaimer: this was copied from a coworkers code but also slightly expanded upon using this article: https://dev.to/bikramjeetsingh/soft-deletes-in-django-a9j
    - it is important to have though
  - set up the middleware that goes along with this 
- created a custom permission on the Dashboard model so only users with the view_admin_dashboard will be able to view the admin dashboard
- thinking about the best design strategy for setting up a admin dashboard and a normal user dashboard
  - considering whether using a proxy model is worth the effor
    - the one problem with using a proxy model is it would force the number of sections on the dashboard for both types to be the same
  - another option would be to use an abstract class
    - the problem with that is there are not enough shared fields to justify using an abstract class
  - I think that the best way to allow for extendability in the future is to set up two seperate models one for admin and one for regular users
- this also brings up the question of what if a user wants to arbitarly add a section to their dashboard, are we going to restrict them to only x amount, another question is will we need to create another model to serve as the dashboard sections?
  - this would give the developer me more control and also there is less room for error using foreign keys than using string values to set the sections
  - could also set up the initial queryset for the second model to return the data that would be first displayed to the user on page reload
  - could also add fields that affect the filter depending on what the user selects during their session
    - this would set up the sections to display the data the user was last view, rather than reverting the data back to presets on page reload
      - there would only be so much we could do with though, particularly when we are using AlpineJS to change graphs depending on selection of another section
        - the whole idea of the app is to create in interactive dashboard where a user can select "full_sun" on one section and then a graph in another section would update using django-ninja to display a pie graph for example of full_sun plants broken down by cycle
      - changes done by AlpineJS would not be saved (or maybe the could)
    - would need to build in a reset functionality on a section basis as well as a dashboard basis
      - at the dashboard level it would call the refresh on each foreign key assigned for a section

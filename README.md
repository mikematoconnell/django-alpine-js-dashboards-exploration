# To Run Application

- will need to create a .env file
  - in this file add PERENUAL_API_KEY, for its value either reach out to me or navigate to https://perenual.com/docs/api and create our own

- set up a Python virtual environment: python -m venv .venv
- activate the virtual environment: .venv/scripts/activate
  - while running this if you encounter this error "cannot be loaded because running scripts is disabled on this system."
    - fix: https://stackoverflow.com/questions/64633727/how-to-fix-running-scripts-is-disabled-on-this-system
- once the virtual environment is installed run: pip install requirement.txt
- then run: python manage.py migrate
  - this will generate a sqlite database in the manage.py directory
- then run: python manage.py import_plant_data
  - this will import the plant data from the API
